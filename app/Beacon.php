<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Check;

class Beacon extends Model
{
    protected $fillable = [
        'id', 'place', 'description',
    ];

    public function beacons(){
        return $this->hasMany(Check::class);
    }
}
