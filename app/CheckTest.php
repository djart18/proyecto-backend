<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckTest extends Model
{
    protected $fillable = [
        'clock', 'check', 'uid', 'beacon_uuid', 'name'
    ];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo(User::class);
    }

}
