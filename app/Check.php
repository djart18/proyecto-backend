<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Beacon;

class Check extends Model
{
    protected $fillable = [
        'clock', 'check', 'uid', 'beacon_uuid'
    ];
    public $timestamps = false;

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function check(){

        return $this->belongsTo(Beacon::class);
    }
}
