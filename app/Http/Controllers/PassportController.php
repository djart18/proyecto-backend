<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;

class PassportController extends Controller
{
    public function register(Request $request){
        $this->validate( $request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:min:6'
        ]);

        $user = User::create([
            'name' => $request->name, 
            'email' => $request->email,
            'password' => bcrypt($request->password),
    ]);

    // $token =  $user->createToken('BeaconToken')->accessToken;
    // return response()->json(['beaconToken' => $token], 200);
    return $user;
    }

    public function login(Request $request){
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
             ];
         // imprime la Request
       //   dd($credentials);

       $persona = \DB::table('users')->where('email', $request->email)->first();
        if($persona!=null){
                // if($persona->password==$request->password){
                //     return $persona;
                // }
                // return $persona->password;
               if($persona->password==$request->password){
                $persona = \DB::table('users')->where('email', $request->email)->get();
                return $persona;
               }else{
                return response()->json(['error' => 'Incorrect Credentials'], 401);
               }
        } else{
            return response()->json(['error' => 'Incorrect Credentials'], 401);
        }
            //    if(auth()->attempt($credentials)){
            //        return response()->json([ auth()->user() ], 200);
            //     } 
            //      else {
            //         return response()->json(['error' => 'Incorrect Credentials'], 401);
            //       }
    }
   

    /* public function applogin(Request $request){
        return User::where('email', $request->input('email') )->get();
    }
    */
    
    public function details(Request $request){
        return $this->login($request);
        // $user = auth()->user();
        // return response()->json(['user' => auth()->user(), 'status' => 200], 200);
    }


}
