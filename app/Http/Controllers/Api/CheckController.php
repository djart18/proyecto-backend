<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Check;
use App\Beacon;
use App\Http\Controllers\OneSignalController;

class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'hola';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Create(Request $request)
    {
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, OneSignalController $onesignal)
    {
        $place = Beacon::where('id', $request->beacon_uuid)->firstOrFail()->place;
        
        if ($request->exists('check_in','uid','check')){
            $onesignal->notifyEntry($place);
            return Check::Create([
                        'clock' => $request->check_in,
                        'check' => $request->check,
                        'uid' => $request->uid,
                        'beacon_uuid' => Beacon::where('id', $request->beacon_uuid)->firstOrFail()->id,
            ]);
        } else { if ($request->exists('check_out', 'uid', 'check')){
            $onesignal->notifyOutput($place);
            return $response = Check::Create([
                        'clock' => $request->check_out,
                        'check' => $request->check,
                        'uid' => $request->uid,
                        'beacon_uuid' => Beacon::where('id', $request->beacon_uuid)->firstOrFail()->id,
                  ]);      
               }
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
