<?php

namespace App\Http\Controllers;

use App\Http\Controllers\OneSignalController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CheckTest;
use App\Beacon;
use App\User;

class CheckTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return CheckTest::all();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($uid)
    {
        return CheckTest::findOrFail($uid);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, OneSignalController $onesignal) {
  
        $salida = 'Salida';
        $entrada = 'Entrada';

        if ($request->exists('check_in')){

            $name = User::where('id', $request->uid)->firstOrFail()->name;

            return CheckTest::Create([
                        'clock' => $request->check_in,
                        'check' => $entrada,
                        'uid' => $request->uid,
                        'name' => $name,
                        'beacon_uuid' => $request->beacon_uuid,
                  ]);

        } else if ($request->exists('check_out')) {

            $name = User::where('id', $request->uid)->firstOrFail()->name;

            return CheckTest::Create([
                        'clock' => $request->check_out,
                        'check' => $salida,
                        'uid' => $request->uid,
                        'name' => $name,
                        'beacon_uuid' => $request->beacon_uuid,
            ]);      
        }
    }

}
