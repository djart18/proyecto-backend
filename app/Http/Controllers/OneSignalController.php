<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OneSignal;

class OneSignalController extends Controller
{
    public function notifyEntry(string $place){

        OneSignal::sendNotificationToAll(
            "Bienvenido a ".$place.", Iteria S.A.S",
            $url = null, 
            $data = null, 
            $buttons = null, 
            $schedule = null
        );
        
        return 'El usuario entró al área';
    }

    public function notifyOutput(string $place){

        OneSignal::sendNotificationToAll(
            "Hasta pronto, gracias por su visita a ".$place, 
            $url = null, 
            $data = null, 
            $buttons = null, 
            $schedule = null
        );
        
        return 'El usuario dejó el área';
    }
}
