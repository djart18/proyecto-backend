<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Person extends JsonResource
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    
     public function toArray($request)
    {
        return [
            'id' => $this.id,
            'name' => $this.name,
            'email' => $this.email,
            'activities' => $this->when($request->get('include') == 'activities', ActivityResource::collection($this->user_id)),
        ];
    }
}
