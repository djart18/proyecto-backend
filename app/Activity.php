<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Activity;

class Activity extends Model
{  
    protected $fillable = [
        'user_id', 'name', 'description', 'estado', 'cliente', 'avance', 'proyecto',
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }

    }
