<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public function Activities()
    {
        return $this->hasMany(Activity::class);
    }
}
