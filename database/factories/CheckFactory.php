<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Check;
use App\User;
use Faker\Generator as Faker;

$factory->define(Check::class, function (Faker $faker) {
    return [
            'user_id' => $faker->randomElement(User::get('id')),
            'check_in' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
            'check_out' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
    ];
});
