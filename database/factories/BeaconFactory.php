<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Beacon;
use Faker\Generator as Faker;

$factory->define(Beacon::class, function (Faker $faker) {
    return [
        'place' => $faker->randomElement(['Beacon_Entrada', 'Beacon_Salida']),
        'description' => $faker->text(40),
    ];
});
