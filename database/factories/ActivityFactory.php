<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Activity;
use Faker\Generator as Faker;
use App\User;

$factory->define(Activity::class, function (Faker $faker) {
    return [
        'user_id' =>  function() {return factory(User::class)->create()->id;},
        'name' => $faker->text(8),
        'cliente' => $faker->text(10), 
        'description' => $faker->text(75),
        'avance' => $faker->randomFloat($nbMaxDecimals = 3, $min = 0, $max = 100),
        'proyecto' => $faker->text(10),
        'estado' => $faker->randomElement(['Inicializado', 'En desarrollo', 'Finalizado']),
    ];
});
