<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('clock');
            $table->text('check');
            $table->integer('uid');
            $table->string('name');
            $table->uuid('beacon_uuid');
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_tests');
    }
}
