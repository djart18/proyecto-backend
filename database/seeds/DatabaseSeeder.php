<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(UserTableSeeder::class);
       //  $this->call(ActivitiesTableSeeder::class);
        
        $this->call(ActivitiesTableSeeder::class);
        $this->call(BeaconsTableSeeder::class);
        $this->call(UsersTableSeeder::class);

    }
}
