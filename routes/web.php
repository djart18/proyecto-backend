<?php

use App\User;
use App\Check;
use App\CheckTest;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Activity;
use App\Beacon;
use App\Http\Controllers\OneSignalController;
use App\Http\Controllers\PassportController;
use App\Http\Resources\CheckCollection;
use App\Http\Resources\CheckResource;
use App\Http\Resources\BeaconResource;
// Route::get('/', function () {

//     return User::where('name', '=','Juan Carlos Herreraa')->firstOrFail()->id;

// });

Route::get('/act', function () {
    return Activity::all();
});

Route::get('/users', function () {
    return User::all();
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/checks', function() {
    return CheckResource::collection(Check::paginate(20));
});

Route::get('/regions', function() {
    return BeaconResource::collection(Beacon::get());
});

Route::get('/rangofechas/{fecha}', function( $fecha){
    return Check::where('clock', '>=', $fecha)->get();
});

/*Login app Ionic*/
Route::get('/loginapp', 'PassportController@applogin');

/*Buscar actividades por id de usuario*/
Route::get('/{id}', function ($id) {
    $user = User::findOrFail($id);
    return $user->activities;
});

Route::get('entry', 'OneSignalController@notifyEntry');

Route::get('output', 'OneSignalController@notifyOutput');

// Route::post('register', 'PassportController@register');
 
Route::get('/email/{val}/password/{pas}',function($val,$pas){

    $credentials = [
        'email' => $val->email,
        'password' =>$pas->password,
    ];

if(auth()->attempt($credentials)){
    return response()->json([ auth()->user() ], 200);
 } else {
    return response()->json(['error' => 'Incorrect Credentials'], 401);
  }


    // return 'correo: '.$val . ' ' .'jajajaaj'.$pas;
});