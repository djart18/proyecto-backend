<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Api')->group( function(){

    Route::apiResource('Users', 'PersonController');
});

/* Route::middleware('auth:api')->namespace('Api')->group( function(){
    Route::apiResource('Checks', 'CheckController');
}); */

/* Route::namespace('Api')->group(function(){
    Route::apiResource('Checks', 'CheckController');
}); */

Route::namespace('Api')->group(function(){
    Route::apiResource('Activities', 'ActivitiesController');
});

Route::namespace('Api')->group(function(){
    Route::apiResource('Beacon', 'BeaconController');
});

// Ruta para revisar las credenciales del usuario y permitir o denegar acceso
Route::post('login', 'PassportController@login');


// Ruta para recuperar los datos de un usuario autorizado
Route::post('user', 'PassportController@details');

// Ruta para registrar un usuario en la aplicacion
Route::post('register', 'PassportController@register');

// Ruta para enviar los Checks
Route::post('checktest', 'CheckTestController@store');

// Ruta para consultar los Checks
Route::get('checktest', 'CheckTestController@index');

